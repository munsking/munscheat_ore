class MunsCheat{

  constructor(){
    this._init();
    this.interval = 200;
    this.miner = 0;
    this.upgrader = 0;
    this.fighter = 0;
    this.quester = 0;
    this.clicked = [];
    this.interval = 200;
  }

  _init(){
    this.mine(this);
    this.upgrade(this);
    this.fight(this);
  }
  
  mine(me){
    if(me.miner > 0){
      clearInterval(me.miner);
      me.miner = 0;
      return;
    }
    me.miner = setInterval(function(){
      if($(".weak-spot")){
        $(".weak-spot").click();
      }
      if($(".gold-nugget-container")){
        $(".gold-nugget-container").click()
      }
    },me.interval);
  }
  
  upgrade(me){
    if(me.upgrader > 0){
      clearInterval(me.upgrader);
      me.upgrader = 0;
      return;
    }
    me.upgrader = setInterval(function(){
      if($(".upgrade:not(.not-enough)")){
        $(".upgrade:not(.not-enough)").click()
      }
      var buildArr=document.getElementsByClassName("building-wrapper")
      for(var building in buildArr){
        if(typeof(buildArr[building].querySelector) == "function"){
	        if(buildArr[building].querySelector(".not-enough")){
	        }else{
		        buildArr[building].querySelector(".building").click();
	        }
        }
      }
    },me.interval);
  }
  
  fight(me){
    if(me.fighter > 0){
      clearInterval(me.fighter);
      me.fighter = 0;
      return;
    }
    me.fighter = setInterval(function(){
    if($(".first-boss-encounter-container")){
      delete me.clicked["boss-approaching"];
      return;
    }
      if($(".manual-attack")){
        $(".manual-attack").click()
      }
      if($(".boss-approaching") && !me.clicked.find(element => element == "boss-approaching")){
        $(".boss-approaching").click()
        me.clicked.push("boss-approaching");
      }
      if($(".quest-success")){
        $(".quest-success").click()
      delete me.clicked[me.clicked.indexOf("boss-approaching")];
      }
    },me.interval);
  }
  quest(me){
    if(me.quester > 0){
      clearInterval(me.quester);
      me.quester = 0;
      return;
    }
    me.quester = setInterval(function(){
        if(document.querySelectorAll(".modal-action")){
          var maArr = document.querySelectorAll(".modal-action");
          if(maArr){
            for( var a in maArr){
              if(maArr[a].textContent && maArr[a].textContent.indexOf('Redo Quest') > -1){
                maArr[a].click()
			          $('.tab-store').click()
              }
            }
          }
        }
    },me.interval);
  }
  
}

mc=new MunsCheat();
